API
====================================



.. toctree::
    :maxdepth: 1
    :caption: Contents:

    actions.rst
    dialogs.rst
    entryline.rst
    grid.rst
    icons.rst
    menubar.rst
    pyspread.rst
    settings.rst
    toolbar.rst
    widgets.rst
    workflows.rst
    model/index.rst
    lib/index.rst

